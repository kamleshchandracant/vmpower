

# VM Power Management 
  This VM Power Management solution to save azure expenditure on running compute. These infrastructures will deploy by azure product team via azure devops pipeline. All reports will be available in sharepoint site for stakeholder. The SharePoint site will NOT be managed by Azure Product Team. 
## Description 
  This solution designed to scan authorised subscriptions for matching compute resources to perform desire operation based on matching system tags. An azure automation account will be used to manage operation and schedule jobs. 
 
  The runbook “vmpower” is an azure automation runbook will be configured to run on schedule jobs (figure 1.2). This runbook expects two parameters “OnTime” and value either “12/5” or “12/7”. If any compute tags get match, then start/stop operation will perform based on schedule jobs. Upon completion of successful job this system will generate a report and will be saved in central storage (SharePoint Site). 

  Any CSV file from system will be deleted after completion of the job and any unmatching tags compute resource will NOT affected.  

## Prerequisites
    - Microsoft Azure Subscription
    - Automation account 
    - Runbook 
    - Schedule job
    - SPN ( RunAsAccount ) 
      - Required permission:
        -	Virtual machine Contributor role 
    -	Assign Tags on Compute Resources
    -	SharePoint site & Dedicated Sharepoint SPN 
    -	New SPN for SharePoint connection (This details will be saved in automation account as variables and runbook will use it when its required )



##  VM’s will start and stop operation will perform on below schedule time. 

| Tag   | Value |	Description |
| ------| ------|-----------------|   
| 24/7	| 24    |  Hours a Day 7 Days a Week |
| 12/5	| 12    |  Hours a Day 5 Days a Week (07:00 – 19:00 Mon – Fri) |
| 12/7	| 12    |  Hours a Day 7 Days a Week (07:00 – 19:00 Everyday) |


There are four jobs will be configured in automation account via azure pipeline. 

1.	“start-12hours5days-schedule”:  This job designed to run on scheduled time (7:00 AM - Mon -FRI ) with below parameters, These values will be configured with this job. 

parameter: 
Action: start
TagName: onTime
TagValue: 12/5 

2.	“stop-12hours5days-schedule”:  This job designed to run on scheduled time (7:00 PM - Mon -FRI ) with below parameters, These values will be configured with this job.

parameter: 
Action: stop
TagName: onTime
TagValue: 12/5 

3.	“start-12hours7days-schedule”:  This job designed to run on scheduled time (7:00 AM – everyday) with below parameters, these values will be configured with this job.

parameter: 
Action: start
TagName: onTime
TagValue: 12/7 

4.	“start-12hours7days-schedule”:  This job designed to run on scheduled time (7:00 PM – everyday) with below parameters, these values will be configured with this job.

parameter: 
Action: stop
TagName: onTime
TagValue: 12/7 


## Requirement 


## Prior to Deployment – For Ops Team

Need to request new SPN via Jira, This SPN will use by “Azure automation account” to write a report to SharePoint document folder. 

A.	Create Document Folder in Sharepoint 
    a.	Log in to Sharepoint site 
    b.	Click on “New” then Select “Document Library” option, Give Folder Name .e.g. “vmpowerreport” 

B.	Give SPN permission to access SharePoint site  
    1.	Go to: https://XXXXX.sharepoint.com/_layouts/15/appinv.aspx
    2.	Search for SPN 
    3.	Add domain name e.g. “hmrc.gov.uk”
    4.	Add below script In XML filed to give permission

         ```xml
        <AppPermissionRequest Scope="http://sharepoint/content/sitecollection/web/list" 
          Right="Write" />
        </AppPermissionRequests>
        ```
    
    5.	Click on create
    6.	Pop-up box will prompt on your screen, select the document folder which you created. 
    7.	Click on “trust it”


## After Deployment – For Developer 

A . Configure RunAsAccount In Automation Account

    1.	Log In to: Azure Account, go to “vmpowermanagemet” Automation account 
    2.	Go to Account settings in Left Pane
    3.	Click on “Run As accounts”
    4.	Click on “Create”
B.	Give subscription level permission, this permission will allow to perform vm start/stop operation. 
    1.	Login to Azure portal & Go to “Subscriptions”
    2.	Select subscription 
    3.	Click on “Access Control (IAM)”
    4.	Search “RunAsAccount” and Select
C.	Create Variables in Automation account (Figure 1.8)
    a.	Login to Azure Account & search for vm power management Automation account
    b.	Click on “Add variable” 
    c.	Add all sharepoint variables 




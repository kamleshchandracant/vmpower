workflow vmpower
{
  Param(
        [Parameter(Mandatory=$true)]
        [String]
        $TagName,
        [Parameter(Mandatory=$true)]
        [String]
        $TagValue,
        [Parameter(Mandatory=$true)]
        [String]
        $Action
        )
     
    $connectionName = "AzureRunAsConnection";
 
    try
    {
        Disable-AzContextAutosave -Scope Process

        # Get the connection "AzureRunAsConnection "
        $servicePrincipalConnection=Get-AutomationConnection -Name $connectionName        
 
        "Logging in to Azure..."
        Connect-AzAccount `
            -ServicePrincipal `
            -TenantId $servicePrincipalConnection.TenantId `
            -ApplicationId $servicePrincipalConnection.ApplicationId `
            -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint 
    }
    catch {
 
        if (!$servicePrincipalConnection)
        {
            $ErrorMessage = "Connection $connectionName not found."
            throw $ErrorMessage
        } else{
            Write-Error -Message $_.Exception
            throw $_.Exception
        }
    }

    $subscriptions = (Get-AzSubscription).Id
    write-output "+++++++++++"

   

       foreach ($sub in $subscriptions)
    {

     Write-Output "Working on  : $($sub)"; 
     $vms = Get-AzResource -TagName $TagName -TagValue $TagValue | where {$_.ResourceType -like "Microsoft.Compute/virtualMachines"}
       
       if(!$VMs) {

           Write-Output "No matched VMs were found in your subscription."
           Write-Output "---------------------------------------  :"

        } else {

                    inlineScript {
                    $result =@()
                    $details=@()
                   # $details.add("Name","1")
                    #$details.add("Status","2")
                    #$details.add("Subscription","3")
                    #$details.add("SubscriptionID","4")
                     }

                    Foreach -Parallel ($vm in $vms){
                        
                        if($Action-eq "stop"){
                            Write-Output "---------------------------------------  :"
                            Write-Output "Stopping : $($vm.Name)";  
                            
                            Write-Output "---------------------------------------  :   "
                            
                                Stop-AzVM -Name $vm.Name -ResourceGroupName $vm.ResourceGroupName -Force; 
                            
                                $details = @{            

                                            Name                = $vm.Name
                                            Status              = "Stopped"
                                            Subscription        = $($sub).Name
                                            SubscriptionID        = $($sub)
                                }
                                 $vmdetails = New-Object PSObject -Property $details 
                                 $result +=$vmdetails
                            
                        }
                        elseif ($Action -eq "start"){
                            Write-Output "---------------------------------------  :"
                            Write-Output "Starting : $($vm.Name)";    
                            
                            Write-Output "---------------------------------------  :"    
                            
                            Start-AzVM -Name $vm.Name -ResourceGroupName $vm.ResourceGroupName; 
                            

                                    $details = @{            
                                                
                                                    Name                = $vm.Name
                                                    status              = "Started"
                                                    subscription        = $($sub).Name
                                                    subscriptionID     = $($sub)

                                        }
                                        $vmdetails = New-Object PSObject -Property $details 
                                        $result +=$vmdetails
                            
                           
                        }
                     Write-Output "-------------Write-CSV-------------------------  :"
                     $result | export-csv -Path VMInfo.csv -NoTypeInformation -Encoding UTF8
                     
                    } 
                    Write-Output "--------------CSV OutPut-------------------------  :"
                   inlineScript {
                    $result | Format-Table -AutoSize
                   }
                  
        
            }  
    }
        
}

    
    
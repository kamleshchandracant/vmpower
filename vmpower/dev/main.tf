terraform {
  backend "azurerm" {
  }
}

provider "azurerm" {
  features {}
  skip_provider_registration = true
}

provider "time" {}

module "vmpower" {
  source = "../modules"

  resource_group_name        = "N049CLOrgpVmpowermanagement"
  location                   = "uksouth"
  azurerm_automation_account = "n049cloaaavmpower"
  morning                    = "07:00"
  evening                    = "19:00"
  weekDays12-5               = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
  weekDays12-7               = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
  environment                = "dev"
  billingid                  = "CLO"
  projectname                = "vmpowermanagement"
  actiongroup = "N049CLO_APD_vmpower_actiongroup"
  stakeholderemail = "nibedita.sharma@hmrc.gov.uk"
  storageaccountlogs = "n49clostovmpower" 
}

#Find Existing RG
data "azurerm_resource_group" "rg" {
  name     = var.resource_group_name
}
resource "azurerm_automation_account" "automation_account" {
  name                = var.azurerm_automation_account
  resource_group_name = var.resource_group_name
  sku_name            = "Basic"
  location     = var.location
  tags = {
    BillingID = var.billingid
    Project   = var.projectname
  }

}

module "runbook" {
    depends_on = [azurerm_automation_account.automation_account]
    source = "./runbook"
    resource_group_name = var.resource_group_name
    location = var.location
    azurerm_automation_account = azurerm_automation_account.automation_account.name
    morning =  var.morning
    evening =  var.evening
    weekDays12-5 = var.weekDays12-5
    weekDays12-7 = var.weekDays12-7
    environment = var.environment
    billingid = var.billingid
    projectname = var.billingid
    storageaccountlogs = var.storageaccountlogs
    
}
module "alerts" {
    depends_on = [azurerm_automation_account.automation_account]
    source = "./alerts"
    resource_group_name = var.resource_group_name
    azurerm_automation_account = var.azurerm_automation_account
    actiongroup = var.actiongroup
    stakeholderemail = var.stakeholderemail
    automation_accountid = [azurerm_automation_account.automation_account.id]
    billingid = var.billingid
    projectname = var.billingid

}
module "storelogs" {
    depends_on = [azurerm_automation_account.automation_account]
    source = "./storelogs"
    resource_group_name = var.resource_group_name
    location = var.location
    automation_accountid = azurerm_automation_account.automation_account.id
    billingid = var.billingid
    projectname = var.billingid
    storageaccountlogs = var.storageaccountlogs 
}
variable "resource_group_name" {}
variable "azurerm_automation_account" {type = string}
variable "morning" {type = string}
variable "evening" {type = string}
variable "weekDays12-5" {type = list(string)}
variable "weekDays12-7" {type = list(string)}
variable "storageaccountlogs" {}


resource "time_offset" "next_day" {
  offset_days = 1
}

locals {
  morning = var.morning
  evening = var.evening
  update_date = substr(time_offset.next_day.rfc3339, 0, 10)
  update_timezone = "Europe/London"
}
# schedule the job for 12 hours 5 days (Mon -Friday) 

resource "azurerm_automation_schedule" "start-schedule12h5days" {
  name                    = "start-12hours5days-schedule"
  resource_group_name = var.resource_group_name
  automation_account_name = var.azurerm_automation_account
  frequency               = "Week"
  interval                = 1
  timezone                = "Europe/London"
  start_time              = "${local.update_date}T${local.morning}:00+00:00"
  description             = "12hours5days-schedule"
  week_days               = var.weekDays12-5
}

resource "azurerm_automation_schedule" "stop-schedule12h5days" {
  depends_on = [azurerm_automation_schedule.start-schedule12h5days]
  name                    = "stop-12hours5days-schedule"
  resource_group_name = var.resource_group_name
  automation_account_name = var.azurerm_automation_account
  frequency               = "Week"
  interval                = 1
  timezone                = "Europe/London"
  start_time              = "${local.update_date}T${local.evening}:00+00:00"
  description             = "12hours5days-schedule"
  week_days               = var.weekDays12-5
}

resource "azurerm_automation_schedule" "start-schedule12h7days" {
  depends_on = [azurerm_automation_schedule.stop-schedule12h5days]
  name                    = "start-12hours7days-schedule"
  resource_group_name = var.resource_group_name
  automation_account_name = var.azurerm_automation_account 
  frequency               = "Week"
  interval                = 1
  timezone                = "Europe/London"
  start_time              = "${local.update_date}T${local.morning}:00+00:00"
  description             = "12hours5days-schedule"
  week_days               = var.weekDays12-7
}

resource "azurerm_automation_schedule" "stop-schedule12h7days" {
  depends_on = [azurerm_automation_schedule.start-schedule12h7days]
  name                    = "stop-12hours7days-schedule"
  resource_group_name = var.resource_group_name
  automation_account_name = var.azurerm_automation_account 
  frequency               = "Week"
  interval                = 1
  timezone                = "Europe/London"
  start_time              = "${local.update_date}T${local.evening}:00+00:00"
  description             = "12hours7days-schedule"
  week_days               = var.weekDays12-7
}
#--------------------------Schedule jobslink to runbook ---------------------------------------

resource "azurerm_automation_job_schedule" "start-job12hours5days" {
  depends_on = [azurerm_automation_schedule.start-schedule12h5days]
  resource_group_name = var.resource_group_name
  automation_account_name = var.azurerm_automation_account
  schedule_name           = azurerm_automation_schedule.start-schedule12h5days.name
  runbook_name            = "vmpower-runbook"

  parameters = {
    tagname = "OnTime"
    tagvalue = "12/5"
    action = "start"

  }
}

resource "azurerm_automation_job_schedule" "stop-job12hours5days" {
  depends_on = [azurerm_automation_schedule.stop-schedule12h5days]
  resource_group_name = var.resource_group_name
  automation_account_name = var.azurerm_automation_account
  schedule_name           = azurerm_automation_schedule.stop-schedule12h5days.name
  runbook_name            = "vmpower-runbook"
  parameters = {
    tagname = "OnTime"
    tagvalue = "12/5"
    action = "stop"
  }
}

resource "azurerm_automation_job_schedule" "start-job12hours7days" {
  depends_on = [azurerm_automation_schedule.start-schedule12h7days]
  resource_group_name = var.resource_group_name
  automation_account_name = var.azurerm_automation_account 
  schedule_name           = azurerm_automation_schedule.start-schedule12h7days.name
  runbook_name            = "vmpower-runbook"

  parameters = {
    tagname = "OnTime"
    tagvalue = "12/7"
    action = "start"
  }
}

resource "azurerm_automation_job_schedule" "stop-job12hours7days" {
  depends_on = [azurerm_automation_schedule.stop-schedule12h7days]
  resource_group_name = var.resource_group_name
  automation_account_name = var.azurerm_automation_account 
  schedule_name           = azurerm_automation_schedule.stop-schedule12h7days.name
  runbook_name            = "vmpower-runbook"

  parameters = {
    tagname = "OnTime"
    tagvalue = "12/7"
    action = "stop"

  }
}
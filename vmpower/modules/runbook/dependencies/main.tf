variable "resource_group_name" {}
variable "azurerm_automation_account" {type = string}
########### Install Dependencies powershell modules  ###########

resource "azurerm_automation_module" "azaccounts" {
  name                    = "Az.Accounts"
  resource_group_name     = var.resource_group_name
  automation_account_name = var.azurerm_automation_account

  module_link {
    uri = "https://psg-prod-eastus.azureedge.net/packages/az.accounts.1.9.5.nupkg"
  }
}

#Install az compute

resource "azurerm_automation_module" "azcompute" {
  depends_on = [azurerm_automation_module.azaccounts]
  name                    = "Az.Compute"
  resource_group_name     = var.resource_group_name
  automation_account_name = var.azurerm_automation_account

  module_link {
    uri = "https://psg-prod-eastus.azureedge.net/packages/az.compute.4.5.0.nupkg"
  }
}

resource "azurerm_automation_module" "azresources" {
  depends_on = [azurerm_automation_module.azcompute]
  name                    = "Az.Resources"
  resource_group_name     = var.resource_group_name
  automation_account_name = var.azurerm_automation_account

  module_link {
    uri = "https://psg-prod-eastus.azureedge.net/packages/az.resources.2.5.1.nupkg"
  }
}

resource "azurerm_automation_module" "azstorage" {
  depends_on = [azurerm_automation_module.azcompute]
  name                    = "Az.Storage"
  resource_group_name     = var.resource_group_name
  automation_account_name = var.azurerm_automation_account

  module_link {
    uri = "https://psg-prod-eastus.azureedge.net/packages/az.storage.2.7.0.nupkg"
  }
}

resource "azurerm_automation_module" "sharepointonline" {
  depends_on = [azurerm_automation_module.azcompute]
  name                    = "SharePointPnPPowerShellOnline"
  resource_group_name     = var.resource_group_name
  automation_account_name = var.azurerm_automation_account

  module_link {
    uri = "https://www.powershellgallery.com/api/v2/package/SharePointPnPPowerShellOnline"
  }
}


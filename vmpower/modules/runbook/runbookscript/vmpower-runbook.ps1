Param(
[Parameter(Mandatory=$true)]
[String]
$TagName,
[Parameter(Mandatory=$true)]
[String]
$TagValue,
[Parameter(Mandatory=$true)]
[String]
$Action
)
####################################### RunAsConnection ################################################
$connectionName = "AzureRunAsConnection"

    try{
         # Get the connection "AzureRunAsConnection "
         $servicePrincipalConnection=Get-AutomationConnection -Name $connectionName        
                     
                    # Wrap authentication in retry logic for transient network failures
                    $logonAttempt = 0
                    while(!($connectionResult) -and ($logonAttempt -le 3)){
                            $LogonAttempt++
                            "Logging in to Azure..."
                            $connectionResult = Connect-AzAccount `
                                                    -ServicePrincipal `
                                                    -TenantId $servicePrincipalConnection.TenantId `
                                                    -ApplicationId $servicePrincipalConnection.ApplicationId `
                                                    -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint 
                    }  
        }catch  {

                    if (!$servicePrincipalConnection){
                            $ErrorMessage = "Connection $connectionName not found."
                            throw $ErrorMessage
                        } else{
                            Write-Error -Message $_.Exception
                            throw $_.Exception
                        }
                }        

################################# Search Compute Tag  ######################################################                           
$subscriptions = (Get-AzSubscription) | Select Id, Name 
$report = @()

foreach ($sub in $subscriptions) {

    Select-AzSubscription $sub.Id
    #Get-AzVM -Status  | Where-Object {$_.tags[$TagName] -eq $TagValue} |Select Name, ResourceGroupName, PowerState | Export-CSV "before_vm_state.csv" -Append -NoTypeInformation
    Write-Output "Working on  : $($sub)"; 
    write-output "---------------------------------------------------------------------------------------"
    $vms = Get-AzVM | Where-Object {$_.tags[$TagName] -eq $TagValue}

    if(!$vms) {

       Write-Output "No matched VMs were found in your subscription."
       Write-Output "--------------------------------------------------------  :"

    } else {
                
                Foreach  ($vm in $vms) {
                           
                            # Get current power state of vm 
                            $vmstate = Get-AzVM -Name $vm.Name -Status | Select PowerState, ResourceGroupName
                            # verifying the passing right operator    
                            if ($Action -eq "start"){

                                            try{
                                                if($vmstate.PowerState -match "Running"){
                                                    $command = "Starting" 
                                                    Write-Output " $($vm.Name) already in Running state"; 
                                                    

                                                 }else{
                                                        $command = "Starting" 
                                                        # Start each of the VMs 
                                                        $StartRtn = Start-AzVM -Name $vm.Name -ResourceGroupName $vm.ResourceGroupName -NoWait -ErrorAction Continue
                                                        if (!$StartRtn.IsSuccessStatusCode) {
                                                            Write-Error ($vm.Name + " failed to start. Error was:") -ErrorAction SilentlyContinue
                                                            $failed_to_start_vm = $vm.Name
                                                            } else{Write-Output ($VM.Name + " has been started")}
                                                    }
                                            }catch {
                                                   
                                                    $ErrorMessage = $_.Exception.Message
                                                    Write-Output "$($vm.Name) :  Error : not re-starting The error message was $ErrorMessage"
                                                    Break
                                                }

                                }elseif ($Action -eq "stop"){

                                    try{
                                            if($vmstate.PowerState -match  "deallocated"){
                                                $command = "stopping"
                                                Write-Output " $($vm.Name) already in deallocated state"; 
                                               
                                             }else{
                                                    $command = "stopping"                                                       
                                                    $StartRtn = Stop-AzVM -Name $vm.Name -ResourceGroupName $vm.ResourceGroupName -Force -NoWait -ErrorAction Continue 
                                                    if (!$StartRtn.IsSuccessStatusCode) {
                                                    # The VM failed to start, so send notice
                                                    Write-Error ($vm.Name + " failed to stop. Error was:") -ErrorAction SilentlyContinue
                                                    $failed_to_stop_vm = $vm.Name
                                                    } else{Write-Output ($VM.Name + " has been stopped")}
                                                }
   
                                        }catch {
                                                    
                                                    $ErrorMessage = $_.Exception.Message
                                                    Write-Output "$($vm.Name) :  Error : not shutting down. The error message was $ErrorMessage"
                                                    Break
                                            }
                                
                                }else{
                                    Write-Output "  Error : $($Action): Operator is not matching"
                                }
  
                                # Store VMS information from this run into the array
                                $details =  @{                                           
                                    SubscriptionName = $sub.name
                                    VMName           = $vm.Name
                                    CurrentStatus    = $vmstate.PowerState
                                    Action           = $command
                                    ResourcegroupName= $vmstate.ResourceGroupName
                                    Failed_to_stop_vm = $failed_to_stop_vm
                                    Failed_to_Stat_vm = $failed_to_start_vm

                                     
                                    }
                                    $report += New-Object PSObject -Property $details 
                 } 

            }            
}

# Export data into report.csv file
$file = (Get-Date -f yyyy-MM-dd_HH-mm-ss)+"-VM-list.csv"    
$report  | Export-CSV -Path $file -Append -NoTypeInformation

if ((Get-Content $file) -eq $Null) {
   Write-Output "No data found in CSV file && Empty file will be deleted"
   Write-Output "---------------------------  :"
   Remove-Item *.csv -Force
}else {
################################# Create Document Library ######################################################
    
            $AppId = Get-AutomationVariable -Name 'sharepoint-client-id'
            $AppSecret = Get-AutomationVariable -Name 'sharepoint-secret-key'
            $url = Get-AutomationVariable -Name 'sharepoint-site-url'
            $documentlibrary = Get-AutomationVariable -Name 'sharepoint-report-folder' 
    try {    
            $connectionstatus = Connect-PnPOnline -Url $url -ClientId $AppId -ClientSecret $AppSecret -ReturnConnection
            Write-host "Site connected".
            $Context= Get-PnPContext # Connect and store the context
            $Web = $Context.Site.RootWeb
            $Context.Load($Web)
            Invoke-PnPQuery -RetryCount 3
            $Web.Title
               

             if (($Web.Title) -eq $null) {

                  Write-Host "Error connecting to SharePoint Online, unable to establish context" -foregroundcolor black -backgroundcolor Red 
                  return

                  }else{
                          if ((Get-PnPList -Identity $documentlibrary) -eq $null) {
                                Write-Host "create library list and sub-folder" -foregroundcolor black -backgroundcolor Green
                                $creatlist = New-PnPList -Title $documentlibrary -Template DocumentLibrary -OnQuickLaunch
                                Write-Host "12-5 folder not exist - Create New " -foregroundcolor black -backgroundcolor Green
                                # $125 perameter - its a hot fix to hide error "The collection has not been initialized" otherwise runbook will re-started 3 timesand go in suspend state
                                $125= Add-PnPFolder -Name "12-5" -Folder "/$documentlibrary" 
                                Write-Host "12-7 folder not exist - Create New " -foregroundcolor black -backgroundcolor Green
                                # $127 perameter - its a hot fix to hide error "The collection has not been initialized" otherwise runbook will re-started 3 timesand go in suspend state 
                                $127 = Add-PnPFolder -Name "12-7" -Folder "/$documentlibrary"
                           }else{

                                  if ((Get-PnPFolderItem $documentlibrary -ItemName "12-5") -eq $null) {
                                            Write-Host "create folder 12-5" -foregroundcolor black -backgroundcolor Green
                                            # $125 perameter - its a hot fix to hide error "The collection has not been initialized" otherwise runbook will re-started 3 timesand go in suspend state
                                            $125= Add-PnPFolder -Name "12-5" -Folder "/$documentlibrary"
                                  }elseif ((Get-PnPFolderItem $documentlibrary -ItemName "12-7") -eq $null){
                                            Write-Host "create folder 12-7" -foregroundcolor black -backgroundcolor Green
                                            # $127 perameter - its a hot fix to hide error "The collection has not been initialized" otherwise runbook will re-started 3 timesand go in suspend state                                         
                                            $127 = Add-PnPFolder -Name "12-7" -Folder "/$documentlibrary"
                                   }
                            }

                 }

        }catch{
            Write-Host "Error connecting to SharePoint Online: $_.Exception.Message" -foregroundcolor black -backgroundcolor Red
            return
        }  

        Write-Output "################################# Upload Report to Sharepoint site : #################################"                  
        try{                
            if ($TagValue -eq "12/7"){
                Write-Output "Upload 12/7 CSV file"
                # $addfile perameter - its a hot fix to hide error "The collection has not been initialized" otherwise runbook will re-started 3 timesand go in suspend state    
                $addfile =  Add-PnPFile -Folder "/$documentlibrary/12-7" -Path $file

            }elseif ($TagValue -eq "12/5"){
                    Write-Output  "Upload 12/5 CSV file"
                    # $addfile perameter - its a hot fix to hide error "The collection has not been initialized" otherwise runbook will re-started 3 timesand go in suspend state    
                    $addfile = Add-PnPFile -Folder "/$documentlibrary/12-5" -Path $file                         
            }
        }catch{
               
               Write-Host "Cant upload file to SharePoint site: $_.Exception.Message" -foregroundcolor black -backgroundcolor Red 
             
        }finally{
            
            Write-Output "Close sharepoint Connection"    
            Disconnect-PnPOnline      
            Write-Output "Remove All CSV files from local disk "  
            Remove-Item *.csv -Force  

    }
         
}
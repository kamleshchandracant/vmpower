variable "resource_group_name" {}
variable "location" {}
variable "azurerm_automation_account" {type = string}
variable "environment" {}
variable "morning" {}
variable "evening" {}
variable "weekDays12-5" {}
variable "weekDays12-7" {}
variable "billingid" {}
variable "projectname" {}
variable "storageaccountlogs" {}

data "local_file" "runbook" {
  filename = "../modules/runbook/runbookscript/vmpower-runbook.ps1"
}

resource "azurerm_automation_runbook" "vmpowerrunbook" {
  name                    = "vmpower-runbook"
  location     = var.location
  resource_group_name = var.resource_group_name
  automation_account_name = var.azurerm_automation_account
  log_verbose             = "true"
  log_progress            = "true"
  description             = "vmpower runbook"
  runbook_type            = "PowerShell"

      publish_content_link {
          
          # this block is mandatory,  however:
          # the terraform will not publish any content to this link
          # the terraform will not use this link to create runbook
          # This link used for hot-fix purpose
         uri = "https://gallery.technet.microsoft.com/scriptcenter/Start-Azure-V2-VMs-6352312e/file/147007/1/Start-AzureV2VMs.ps1"
       
      }
  content = data.local_file.runbook.content
}
########### Install Dependencies powershell modules  ###########
module "dependencies" {
    source = "./dependencies"
    resource_group_name = var.resource_group_name
    azurerm_automation_account = var.azurerm_automation_account

}
############# schedule Jobs ############
module "schedulejobs" {
    depends_on = [azurerm_automation_runbook.vmpowerrunbook] 
    source = "./schedulejobs"
    resource_group_name = var.resource_group_name
    azurerm_automation_account = var.azurerm_automation_account
    morning = var.morning
    evening = var.evening
    weekDays12-5 = var.weekDays12-5
    weekDays12-7 = var.weekDays12-7 
    storageaccountlogs = var.storageaccountlogs
}
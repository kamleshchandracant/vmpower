variable "resource_group_name" {}
variable "location" {}
variable "azurerm_automation_account" {type = string}
variable "environment" {}
variable "morning" {}
variable "evening" {}
variable "weekDays12-5" {}
variable "weekDays12-7" {}
variable "actiongroup" {}
variable "stakeholderemail" {}
variable "billingid" {}
variable "projectname" {}
variable "storageaccountlogs" {}

variable "resource_group_name" {}
variable "azurerm_automation_account" {type = string}
variable "automation_accountid" {}
variable "actiongroup" {}
variable "stakeholderemail" {}
variable "billingid" {}
variable "projectname" {}

##### create action group & Alert to monitor failed runbook
resource "azurerm_monitor_action_group" "actiongroup" {
  name                = var.actiongroup
  resource_group_name = var.resource_group_name
  short_name          = "vmpower"

  
  email_receiver {
    name          = "stakeholderemail"
    email_address = var.stakeholderemail
  }
  tags = {
    BillingID = var.billingid
    Project   = var.projectname
  }
}

resource "azurerm_monitor_metric_alert" "failedalert" {
  name                = "failed runbook jobs alert"
  resource_group_name = var.resource_group_name
  scopes              = var.automation_accountid
  description = "failed to start vmpower runbook"
  enabled = true

  criteria {
    metric_name = "TotalJob"
    metric_namespace = "Microsoft.Automation/automationAccounts"
    operator    = "GreaterThanOrEqual"
    threshold   = 1
    aggregation = "Total"
    

    dimension {
        name     = "Runbook"
        operator = "Include"
        values   = ["vmpower-12-5-start,vmpower-12-5-stop,vmpower-12-7-start,vmpower-12-7-stop"]
      }

    dimension {
        name     = "Status"
        operator = "Include"
        values   = ["Failed"]
      }
  }
 action {
    action_group_id = azurerm_monitor_action_group.actiongroup.id
  }
}


resource "azurerm_monitor_metric_alert" "stoppedalert" {
  name                = "stopped runbook jobs alert"
  resource_group_name = var.resource_group_name
  scopes              = var.automation_accountid
  description = "stopped to start vmpower runbook"
  enabled = true

  criteria {
    metric_name = "TotalJob"
    metric_namespace = "Microsoft.Automation/automationAccounts"
    operator    = "GreaterThanOrEqual"
    threshold   = 1
    aggregation = "Total"
    

    dimension {
        name     = "Runbook"
        operator = "Include"
        values   = ["vmpower-12-5-start,vmpower-12-5-stop,vmpower-12-7-start,vmpower-12-7-stop"]
      }

    dimension {
        name     = "Status"
        operator = "Include"
        values   = ["Stopped"]
      }
  }

 action {
    action_group_id = azurerm_monitor_action_group.actiongroup.id
  }
}


resource "azurerm_monitor_metric_alert" "suspendedalert" {
  name                = "suspended runbook jobs alert"
  resource_group_name = var.resource_group_name
  scopes              = var.automation_accountid
  description = "suspended to start vmpower runbook"
  enabled = true

  criteria {
    metric_name = "TotalJob"
    metric_namespace = "Microsoft.Automation/automationAccounts"
    operator    = "GreaterThanOrEqual"
    threshold   = 1
    aggregation = "Total"
    

    dimension {
        name     = "Runbook"
        operator = "Include"
        values   = ["vmpower-12-5-start,vmpower-12-5-stop,vmpower-12-7-start,vmpower-12-7-stop"]
      }

    dimension {
        name     = "Status"
        operator = "Include"
        values   = ["Suspended"]
      }
  }

 action {
    action_group_id = azurerm_monitor_action_group.actiongroup.id
  }
}
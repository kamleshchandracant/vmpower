variable "resource_group_name" {}
variable "location" {}
variable "automation_accountid" {}
variable "storageaccountlogs" {}
variable "billingid" {}
variable "projectname" {}

######## store logs in storage account
resource "azurerm_storage_account" "storage" {
  name                     = var.storageaccountlogs
  resource_group_name = var.resource_group_name
  location     = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  tags = {
    BillingID = var.billingid
    Project   = var.projectname
  }
 
}

resource "azurerm_monitor_diagnostic_setting" "vmpowerautodiag" {
  depends_on = [azurerm_storage_account.storage]
  name               = "vmpowerautodiag"
  target_resource_id = var.automation_accountid
  storage_account_id = azurerm_storage_account.storage.id
 

  log {
    category = "JobLogs"
    enabled  = true

    retention_policy {
      enabled = true
      days = 90
    }
  }

  metric {
    category = "AllMetrics"

    retention_policy {
      enabled = true
      days = 90
    }
  }
}

terraform {
  backend "azurerm" {
  }
}

provider "azurerm" {
  features {}
  skip_provider_registration = true
}

provider "time" {}

module "vmpower" {
  source = "../modules"

  resource_group_name        = "P056CLOrgpVMPOW"
  location                   = "uksouth"
  azurerm_automation_account = "p056cloaaavmpower"
  morning                    = "07:00"
  evening                    = "19:00"
  weekDays12-5               = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
  weekDays12-7               = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
  environment                = "prod"
  billingid                  = "CLO"
  projectname                = "VM Power Management"
  actiongroup = "P056CLO_sysman_vmpower_actiongroup"
  stakeholderemail = "bhupesh.patel@hmrc.gov.uk"
  storageaccountlogs = "p056clostovmpower" 
  
}